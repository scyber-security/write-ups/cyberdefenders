# [Boss_Of_The_SOC_v3]()


## Contents
- [Questions](#questions)
    1. [Question 1](#question-1) *This is a simple question to get you familiar with submitting answers. What is the name of the company that makes the software that you are using for this competition? Answer guidance: A six-letter word with no punctuation.*
    2. [Question 2](#question-2) *List out the IAM users that accessed an AWS service (successfully or unsuccessfully) in Frothly's AWS environment?*
    3. [Question 3](#question-3) *What field would you use to alert that AWS API activity have occurred without MFA (multi-factor authentication)?*
    4. [Question 4](#question-4) *What is the processor number used on the web servers?*
    5. [Question 5](#question-5) **
    6. [Question 6](#question-6) **
    7. [Question 7](#question-7) **
    8. [Question 8](#question-8) **
    9. [Question 9](#question-9) **
    10. [Question 10](#question-10) **
    11. [Question 11](#question-11) **
    12. [Question 12](#question-12) **
    13. [Question 13](#question-13) **
    14. [Question 14](#question-14) **
    15. [Question 15](#question-15) **
    16. [Question 16](#question-16) **
    17. [Question 17](#question-17) **
    18. [Question 18](#question-18) **
    19. [Question 19](#question-19) **
    20. [Question 20](#question-20) **
    21. [Question 21](#question-21) **
    22. [Question 22](#question-22) **
    23. [Question 23](#question-23) **
    24. [Question 24](#question-24) **
    25. [Question 25](#question-25) **
    26. [Question 26](#question-26) **
    27. [Question 27](#question-27) **
    28. [Question 28](#question-28) **
    29. [Question 29](#question-29) **
    30. [Question 30](#question-30) **
    31. [Question 31](#question-31) **
    32. [Question 32](#question-32) **
    33. [Question 33](#question-33) **
    34. [Question 34](#question-34) **
    35. [Question 35](#question-35) **
    36. [Question 36](#question-36) **
    37. [Question 37](#question-37) **
    38. [Question 38](#question-38) **
    39. [Question 39](#question-39) **
    40. [Question 40](#question-40) **
    41. [Question 41](#question-41) **
    42. [Question 42](#question-42) **
    43. [Question 43](#question-43) **
    44. [Question 44](#question-44) **
    45. [Question 45](#question-45) **
    46. [Question 46](#question-46) **
    47. [Question 47](#question-47) **
    48. [Question 48](#question-48) **
    49. [Question 49](#question-49) **
    50. [Question 50](#question-50) **
    51. [Question 51](#question-51) **
    52. [Question 52](#question-52) **
    53. [Question 53](#question-53) **
    54. [Question 54](#question-54) **
    55. [Question 55](#question-55) **
    56. [Question 56](#question-56) **
    57. [Question 57](#question-57) **
    58. [Question 58](#question-58) **


---

## Questions

### Question 1
###### This is a simple question to get you familiar with submitting answers. What is the name of the company that makes the software that you are using for this competition? Answer guidance: A six-letter word with no punctuation.
> **Answer:** `Splunk`


### Question 2
###### List out the IAM users that accessed an AWS service (successfully or unsuccessfully) in Frothly's AWS environment? Answer guidance: Comma separated without spaces, in alphabetical order. (Example: ajackson,mjones,tmiller)
> **Search Query:** `sourcetype="aws:cloudtrail" user_type=IAMUser | dedup userName | table userName | sort userName`

> **Answer:** `bstoll,btun,splunk_access,web_admin`

I started with a basic query so that I could find the field to filter by; which happens to be `userName` in this instance.

![IAM Users](./images/02-iam_users.pngs)


### Question 3
###### What field would you use to alert that AWS API activity have occurred without MFA (multi-factor authentication)? Answer guidance: Provide the full JSON path. (Example: iceCream.flavors.traditional)
> **Answer:** ``


### Question 4
###### What is the processor number used on the web servers? Answer guidance: Include any special characters/punctuation. (Example: The processor number for Intel Core i7-8650U is i7-8650U.)
> **Answer:** ``


### Question 5
######
> **Answer:** ``


### Question 6
######
> **Answer:** ``


### Question 7
######
> **Answer:** ``


### Question 8
######
> **Answer:** ``


### Question 9
######
> **Answer:** ``


### Question 10
######
> **Answer:** ``


### Question 11
######
> **Answer:** ``


### Question 12
######
> **Answer:** ``


### Question 13
######
> **Answer:** ``


### Question 14
######
> **Answer:** ``


### Question 15
######
> **Answer:** ``


### Question 16
######
> **Answer:** ``


### Question 17
######
> **Answer:** ``


### Question 18
######
> **Answer:** ``


### Question 19
######
> **Answer:** ``


### Question 20
######
> **Answer:** ``


### Question 21
######
> **Answer:** ``


### Question 22
######
> **Answer:** ``


### Question 23
######
> **Answer:** ``


### Question 24
######
> **Answer:** ``


### Question 25
######
> **Answer:** ``


### Question 26
######
> **Answer:** ``


### Question 27
######
> **Answer:** ``


### Question 28
######
> **Answer:** ``


### Question 29
######
> **Answer:** ``


### Question 30
######
> **Answer:** ``


### Question 31
######
> **Answer:** ``


### Question 32
######
> **Answer:** ``


### Question 33
######
> **Answer:** ``


### Question 34
######
> **Answer:** ``


### Question 35
######
> **Answer:** ``


### Question 36
######
> **Answer:** ``


### Question 37
######
> **Answer:** ``


### Question 38
######
> **Answer:** ``


### Question 39
######
> **Answer:** ``


### Question 40
######
> **Answer:** ``


### Question 41
######
> **Answer:** ``


### Question 42
######
> **Answer:** ``


### Question 43
######
> **Answer:** ``


### Question 44
######
> **Answer:** ``


### Question 45
######
> **Answer:** ``


### Question 46
######
> **Answer:** ``


### Question 47
######
> **Answer:** ``


### Question 48
######
> **Answer:** ``


### Question 49
######
> **Answer:** ``


### Question 50
######
> **Answer:** ``


### Question 51
######
> **Answer:** ``


### Question 52
######
> **Answer:** ``


### Question 53
######
> **Answer:** ``


### Question 54
######
> **Answer:** ``


### Question 55
######
> **Answer:** ``


### Question 56
######
> **Answer:** ``


### Question 57
######
> **Answer:** ``


### Question 58
######
> **Answer:** ``
