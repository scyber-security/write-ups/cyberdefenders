# [CyberDefenders - BSides Jeddah CTF](https://ctf.bsidesjeddah.com/)


## Memory Questions

### Question 1
###### What is the SHA256 hash value of the RAM image?
> **Answer:** `5b3b1e1c92ddb1c128eca0fa8c917c16c275ad4c95b19915a288a745f9960f39`

`sha256sum memory.mem`

### Question 2
###### What time was the RAM image acquired according to the suspect system? (YYYY-MM-DD HH:MM:SS)
> **Answer:** `2021-08-06 16:13:23`

`vol3 -f memory.mem windows.info.Info`

### Question 3
###### What volatility2 profile is the most appropriate for this machine. imageinfo will take a long try to figure another way to determine the profile? (ex: Win10x86_14393)
> **Answer:** `Win2016x64_14393` or `Win10x64_10586`

`./vol2 -f memory.mem imageinfo`

### Question 4
###### What is the computer's name?
> **Answer:** `WIN-8QOTRH7EMHC`

`vol3 -f memory.mem windows.envars.Envars | grep -i computer`

### Question 5
###### What is the system IP address?
> **Answer:** `192.168.144.131`

### Question 6
###### How many established network connections were at the time of acquisition?
> **Answer:** `12`

`vol3 -f memory.mem windows.netscan.NetScan | grep ESTABLISHED | wc -l`

### Question 7
###### What is the PID of explorer.exe?
> **Answer:** `2676`

`vol3 -f memory.mem windows.pslist.PsList | grep -i explorer`

### Question 8
###### What is the title of the webpage the admin visited using IE? Two words, one-space
> **Answer:** `Google News`

`./vol2 -f memory.mem --profile=Win2016x64_14393 iehistory`

### Question 9
###### What company developed the program used for memory acquisition?
> **Answer:** `belkasoft`

`vol3 -f memory.mem windows.pslist.PsList` -> RamCapture64.exe

### Question 10
###### What is the administrator user password?
> **Answer:** `52(dumbledore)oxim`

`vol3 -f memory.mem windows.hashdump.Hashdump` -> `3aff70b832f6170bda6f7b641563f60b`

### Question 11
###### What is the version of the WebLogic server installed on the system?
> **Answer:** ``

### Question 12
###### The admin set a port forward rule to redirect the traffic from the public port to the WebLogic admin portal port. What is the public and WebLogic admin portal port number? Format PublicPort:WebLogicPort (22:1337)
> **Answer:** ``

### Question 13
###### The attacker gain access through WebLogic Server. What is the PID of the process responsible for the initial exploit?
> **Answer:** ``

### Question 14
###### What is the PID of the next entry to the previous process? (Hint: ActiveProcessLinks list
> **Answer:** ``

### Question 15
###### How many threads does the previous process have?
> **Answer:** ``

### Question 16
###### The attacker gain access to the system through the webserver. What is the CVE number of the vulnerability exploited?
> **Answer:** ``

### Question 17
###### The attacker used the vulnerability he found in the webserver to execute a reverse shell command to his server. Provide the IP and port of the attacker server? Format: IP:port
> **Answer:** ``

### Question 18
###### The attacker downloaded multiple files from the his own web server. Provide the Command used to download the PowerShell script used for persistence?
> **Answer:** ``

### Question 19
###### What is the MITRE ID related to the persistence technique the attacker used?
> **Answer:** ``

### Question 20
###### After maintaining persistence, the attacker dropped a cobalt strike beacon. Try to analyze it and provide the Publickey_MD5.
> **Answer:** ``

### Question 21
###### What is the URL of the exfiltrated data?
> **Answer:** ``


---

## PCAP Questions

### Question 1
###### What is the victim's MAC address?
> **Answer:** `000c29b7ca91`

### Question 2
###### What is the address of the company associated with the victim's machine MAC address?
> **Answer:** ``

### Question 3
###### hat is the attacker's IP address?
> **Answer:** ``

### Question 4
###### What is the IPv4address of the DNS server used by the victim machine?
> **Answer:** ``

### Question 5
###### What domain is the victim looking up in packet 5648
> **Answer:** `omextemplates.content.office.net`

> **Wireshark Filter:** `frame.number == 5648`


### Question 6
###### What is the server certificate public key that was used in TLS session:
> **Answer:** `64089e29f386356f1ffbd64d7056ca0f1d489a09cd7ebda630f2b7394e319406`

### Question 7
###### What domain is the victim connected to in packet 4085?
> **Answer:** ``

> **Wireshark Filter:** `frame.number == 4085`

### Question 8
###### The attacker conducted a port scan of the victim machine. How many open ports did the attacker find?
> **Answer:** ``

### Question 9
###### Analyse the PCAP using the provided rules. What is the CVE number falsely alerted by Suricata?
> **Answer:** ``

### Question 10
###### What is the command parameter sent by the attacker in packet number 2650?
> **Answer:** ``

### Question 11
###### What is the stream number hich contains email traffic?
> **Answer:** ``

### Question 12
###### What is th victim's email address?
> **Answer:** ``
