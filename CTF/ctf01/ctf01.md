# [CTF01](https://ctf01.cyberdefenders.org/)

## Scenario
You have been called to analyze a compromised Linux server. Figure out how the threat actor gained access, what modifications were applied to the system, and what persistent techniques were utilized. (e.g. backdoors, users, sessions, etc).

---

- [Scenario](#scenario)
- [Pre-Steps](#pre-steps)
- [Questions](#questions)
    1. [Question 1](#question-1) *What is the RHEL version of the operating system?*
    2. [Question 2](#question-2) *How many users had a login shell?*
    3. [Question 3](#question-3) *How many users were allowed to run the SUDO command on the system?*
    4. [Question 4](#question-4) *What was the password of `rossatron` account?*
    5. [Question 5](#question-5) *What was the victim's IP address?*
    6. [Question 6](#question-6) *What service did the attacker use to gain access to the system?*
    7. [Question 7](#question-7) *What was the attacker's IP address?*
    8. [Question 8](#question-8) *What authentication attack did the attacker use to gain access to the system?*
    9. [Question 9](#question-9) *How many accounts was the attacker able to get their password?*
    10. [Question 10](#question-10) *When did the attack start?*
    11. [Question 11](#question-11) *What is the compromised user account used to gain initial access to the system?*
    12. [Question 12](#question-12) *What is the MITRE ID of the technique used to achieve persistence after the intial access?*
    13. [Question 13](#question-13) *What is the CVE number used by the attacker to escalate privileges?*
    14. [Question 14](#question-14) *The attacker dropped a backdoor to achieve persistence. The backdoor received commands from a Gmail account. What is the email used to send commands?*
    15. [Question 15](#question-15) *The attacker downloaded a keylogger to capture users' keystrokes. What is the secret word the attacker was able to exfiltrate?*

---
## Pre-Steps

## Questions

### Question 1
###### What is the RHEL version of the operating system?
> **Answer:** `8.4`

Once we have the image mounted, we would need to navigate to the **/etc** directory of the image. In here, we're looking for the file **redhat-release**. A simple `cat` will give you the RHEL version.

![RHEL Version](./images/01-rhel_version.png)


### Question 2
###### How many users had a login shell?
> **Answer:** `6`

Sticking to the **/etc** directory, we need to have a look at the **passwd** file as this is where the user's login shell is declared. Running the following command will give us all of the users who have a valid shell for loging in:
```bash
grep bash passwd
```

To get a quick count, we can pass the output of the previous command in to `wc`.

![Login Shells](./images/02-logins.png)


### Question 3
###### How many users were allowed to run the SUDO command on the system?
> **Answer:** `2`

There are a number of places a user can have privileges assigned to them when it comes to `sudo`:
 * **/etc/sudoers**
 * **/etc/sudoers.d/<username>**
 * **/etc/group**

There are no files in the **sudoers.d** directory, and no additional users declared in the **sudoers**, all except for the `wheel` group. This means that we'll need to take a look at the **/etc/group** file to determine how many users have the ability to run `sudo` on the system.

![sudo users](./images/03-sudoers.png)


### Question 4
###### What was the password of `rossatron` account?
> **Answer:** `rachelgreen`

There are a few steps to get the answer to this. First off we need both the **passwd** and **shadow** file from the **/etc** directory. Once we have those, we'll need to run the `unshadow` command which is included with the *john the ripper* package.

The command to run this is: `unshadow /mnt/etc/passwd /mnt/etc/shadow > unshadowed`.

Now that we have the unshadowed file, it's time to start cracking the password. The command to do this is: `john --users=rossatron --wordlist=rockyou.txt unshadowed`.

![Password Cracking](./images/04-cracked_password.png)


### Question 5
###### What was the victim's IP address?
> **Answer:** `192.168.196.129`

This is quite a simple question to get the answer to. In the directory **/var/log** there is a file which contains messages from the system, some of which refer to DHCP and the allocation of the IP address for the machine This file is **messages**. Running the command `grep dhcp messages | grep address` will give you the IP address of the system.

![Victim IP](./images/05-victim_ip.png)


### Question 6
###### What service did the attacker use to gain access to the system?
> **Answer:** `ssh`




### Question 7
###### What was the attacker's IP address?
> **Answer:** `192.168.196.128`


### Question 8
###### What authentication attack did the attacker use to gain access to the system?
> **Answer:** `brute-force`


### Question 9
###### How many accounts was the attacker able to get their password?
> **Answer:** `2`


### Question 10
###### When did the attack start?
> **Answer:** `23/08/2021`


### Question 11
###### What is the compromised user account used to gain initial access to the system?
> **Answer:** `chandler`


### Question 12
###### What is the MITRE ID of the technique used to achieve persistence after the intial access?
> **Answer:** `T1098.004`


### Question 13
###### What is the CVE number used by the attacker to escalate privileges?
> **Answer:** `CVE-2021-3560`


### Question 14
###### The attacker dropped a backdoor to achieve persistence. The backdoor received commands from a Gmail account. What is the email used to send commands?
> **Answer:** `cdefender16@gmail.com`


### Question 15
###### The attacker downloaded a keylogger to capture users' keystrokes. What is the secret word the attacker was able to exfiltrate?
> **Answer:** `haveagoooodday`
