# [DeepDive](https://cyberdefenders.org/labs/78)


#### Scenario
You have given a memory image for a compromised machine. Analyze the image and figure out attack details.

---

## Contents
- [Scenario](#scenario)
- [Questions](#questions)
    1. [Question 1](#question-1) *What profile should you use for this memory sample?*
    2. [Question 2](#question-2) *What is the KDBG virtual address of the memory sample?*
    3. [Question 3](#question-3) *There is a malicious process running, but it's hidden. What's its name?*
    4. [Question 4](#question-4) *What is the physical offset of the malicious process?*
    5. [Question 5](#question-5) *What is the full path (including executable name) of the hidden executable?*
    6. [Question 6](#question-6) *Which malware is this?*
    7. [Question 7](#question-7) *The malicious process had two PEs injected into its memory. What's the size in bytes of the Vad that contains the largest injected PE?*
    8. [Question 8](#question-8) *This process was unlinked from the ActiveProcessLinks list. Follow its forward link. Which process does it lead to?*
    9. [Question 9](#question-9) *What is the pooltag of the malicious process in ascii?*
    10. [Question 10](#question-10) *What is the physical address of the hidden executable's pooltag?*


---

## Questions

### Question 1
###### What profile should you use for this memory sample?
> **Answer:** `Win7SP1x64_24000`


### Question 2
###### What is the KDBG virtual address of the memory sample?
> **Answer:** `0xf80002bef120`


### Question 3
###### There is a malicious process running, but it's hidden. What's its name?
> **Answer:** `vds_ps.exe`


### Question 4
###### What is the physical offset of the malicious process?
> **Answer:** `0x000000007d336950`

python vol.py -f /images/banking-malware.vmem --profile=Win7SP1x64_24000 psxview

### Question 5
###### What is the full path (including executable name) of the hidden executable?
> **Answer:** `C:\Users\john\AppData\Local\api-ms-win-service-management-l2-1-0\vds_ps.exe`

python vol.py -f /images/banking-malware.vmem --profile=Win7SP1x64_24000 dlllist --offset=0x000000007d336950

### Question 6
###### Which malware is this?
> **Answer:** `emotet`


### Question 7
###### The malicious process had two PEs injected into its memory. What's the size in bytes of the Vad that contains the largest injected PE? Answer in hex, like: 0xABC
> **Answer:** `0x36FFF`

python vol.py -f /images/banking-malware.vmem --profile=Win7SP1x64_24000 malfind --offset=0x000000007d336950
python vol.py -f /images/banking-malware.vmem --profile=Win7SP1x64_24000 vadinfo --offset=0x000000007d336950 --addr=0x2a10000
python vol.py -f /images/banking-malware.vmem --profile=Win7SP1x64_24000 vadinfo --offset=0x000000007d336950 --addr=0x2a80000

1CFFF
36FFF


### Question 8
###### This process was unlinked from the ActiveProcessLinks list. Follow its forward link. Which process does it lead to? Answer with its name and extension
> **Answer:** ``

cc(offset=0x000000007d336950, physical=True)


### Question 9
###### What is the pooltag of the malicious process in ascii? (HINT: use volshell)
> **Answer:** ``


### Question 10
###### What is the physical address of the hidden executable's pooltag? (HINT: use volshell)
> **Answer:** ``
