# [Inside](https://cyberdefenders.org/labs/65)

#### Scenario
One of the SOC analysts took a memory dump from a machine infected with a meterpreter malware. As a Digital Forensicators, your job is to analyze the dump, extract the available indicators of compromise (IOCs) and answer the provided questions.


---
## Questions

### 1. What is the SHA1 hash of triage.mem (memory dump)?
> `c95e8cc8c946f95a109ea8e47a6800de10a27abd`

Simply ran `sha1sum Triage-Memory.mem`.


### 2. What volatility profile is the most appropriate for this machine? (ex: Win10x86_14393)
> `Win7SP1x64`

Ran `volatility imageinfo Triage-Memory.mem`. Tried the first of the profiles which was listed.

![The Profile](./images/02-profile.png)


### 3. What was the process ID of notepad.exe?
> `3032`

Command ran: `volatility --profile Win7SP1x64 -f Triage-Memory.mem pslist | grep -i notepad`

![Notepad](./images/03-notepad.png)


### 4. Name the child process of wscript.exe.
> `UWkpjFjDzM.exe`

First ran `volatility --profile Win7SP1x64 -f Triage-Memory.mem pstree | grep -i wscript.exe` to get the PID. Then ran `volatility --profile Win7SP1x64 -f Triage-Memory.mem pstree | grep 5116` to get the parent process and all of it's children.

![Childcare](./images/04-child.png)


### 5. What was the IP address of the machine at the time the RAM dump was created?
> `10.0.0.101`

I tried a number of the different plugins to get the correct one to use for this particular image. But when I did find it, I ran the command `volatility --profile Win7SP1x64 -f Triage-Memory.mem netscan` and picked an address in the **Local Address** field that was an RFC-1918 type address.

![Where am I](./images/05-local_ip.png)


### 6. Based on the answer regarding the infected PID, can you determine the IP of the attacker?
> `10.0.0.106`

I found the answer to this one within the same results from the previous question, however I could have just run `volatility --profile Win7SP1x64 -f Triage-Memory.mem netscan | grep UWkpjFjDzM` if to pin point the information.

![The Attacker](./images/06-attacker.png)


### 7. How many processes are associated with VCRUNTIME140.dll?
> `5`


### 8. What is the md5 hash of the potential malware on the system?
> `690ea20bc3bdfb328e23005d9a80c290`

The potential malware is the executable from question 4. First I needed to extract the file using the following command: `volatility --profile Win7SP1x64 -f Triage-Memory.mem procdump -p 3496 --dump-dir .`.

Once that had been dumped, it was a simple case of running the command `md5sum executable.3496.exe`.


### 9. What is the LM hash of Bob's account?
> `aad3b435b51404eeaad3b435b51404ee`

To get to this answer there are a number of step required, the first is to get a list of the registry values in memory: `volatility --profile Win7SP1x64 -f Triage-Memory.mem hivelist`. The values we're interested in are **\SystemRoot\System32\Config\SAM** which is the value for the `-s` flag, and **\REGISTRY\MACHINE\SYSTEM** which is the value for the `-y` flag in the next command.

Next is to dump all of the hashes using this command `volatility --profile Win7SP1x64 -f Triage-Memory.mem hashdump -y 0xfffff8a000024010 -s 0xfffff8a000e66010 > hashes.txt`.

![Hashing](./images/09-hash.png)


### 10. What memory protection constants does the VAD node at 0xfffffa800577ba10 have?
> `PAGE_READONLY`

Ran the following command `volatility --profile Win7SP1x64 -f Triage-Memory.mem vadinfo | grep '0xfffffa800577ba10' -A11` after searching for *memory protection* on the [documentation](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#vadinfo).

![VAD](./images/10-vadinfo.png)


### 11. What memory protection did the VAD starting at 0x00000000033c0000 and ending at 0x00000000033dffff have?
> `PAGE_NOACCESS`

Just like the previous question, but substituting out the value to grep for.


### 12. There was a VBS script that ran on the machine. What is the name of the script? (submit without file extension)
> `vhjReUDEuumrX`

I know that most VBS scripts have the extension **.vbs**, so I ran the command `volatility --profile Win7SP1x64 -f Triage-Memory.mem cmdline | grep -i vbs`.

![VBS](./images/12-vbs.png)


### 13. An application was run at 2019-03-07 23:06:58 UTC. What is the name of the program? (Include extension)



### 14. What was written in notepad.exe at the time when the memory dump was captured?



### 15. What is the short name of the file at file record 59045?
> `EMPLOY~1.XLS`

Ran the command `volatility --profile Win7SP1x64 -f Triage-Memory.mem mftparser --output=body -D output --output-file=dump.body` and used `grep` to find the record inside of **dump.body**

![Employee Info](./images/15-EMPLOY.png)


### 16. This box was exploited and is running meterpreter. What was the infected PID?
> `3496`

Typically, meterpreter runs executables with a random assortment of uppercase and lowercase letters. The answer from question *4* fits that bill quite nicely.
