# [CyberCorp Case 1](https://cyberdefenders.org/labs/74)


#### Scenario
CyberCorp company has been informed that its infrastructure is likely to be compromised, as there are a number of anomalies in its outgoing traffic. The anomalies suggest that a known threat group behind this attack.

CyberCorp's Cybersecurity team isolated one of the potentially compromised hosts from the corporate network and collected artifacts necessary for the investigation: memory dump, OS event logs, registry files, Prefetch files, $MFT file, ShimCache, AmCache, network traffic dumps. You will have to analyze the collected artifacts and answer the questions to complete the investigation.

---

## Contents
- [Scenario](#scenario)
- [Questions](#questions)
    1. [Question 1](#question-1) *What is the build number of the installed Windows version?*
    2. [Question 2](#question-2) *What is the parent process PID of the process, that accepts incoming network connections on the port 1900/UDP?*
    3. [Question 3](#question-3) *What is the IP address of the attacker command and control center, the connection with which was still active at the time of forensic artifacts acquisition?*
    4. [Question 4](#question-4) *What is the PID of the process where malicious code was located at the moment of forensic artifacts acquisition?*
    5. [Question 5](#question-5) *On a compromised system, malicious code, discovered in the previous step, is launched every system start, since the attacker has used one of the persistence techniques. So, what is the name of the autostart entry (those part, that is directly responsible for code execution), used by the attacker for persistence?*
    6. [Question 6](#question-6) *The autostart entry from the previous step is used to launch the script, which in turn leads to the malicious code execution in the memory of the process, which is discussed in [question 4](#question-4). This code is extracted by script from some system place in the encoded form. The decoded value of this string is executable PE-file. How did Microsoft Antivirus detect this file on 2020-06-21?*
    7. [Question 7](#question-7) *The process, mentioned in the [question 4](#question-4), isn't the initial process, where malicious code, described in the previous question, was executed by script from autostart. What is the name of the initial process (in the format program.exe), that is spawned by autostart script and used for further malicious code execution, that subsequently migrates to the address space of the process, mentioned in the [question 4](#question-4)?*
    8. [Question 8](#question-8) **
    9. [Question 9](#question-9) **
    10. [Question 10](#question-10) **
    11. [Question 11](#question-11) **
    12. [Question 12](#question-12) **
    13. [Question 13](#question-13) **
    14. [Question 14](#question-14) **
    15. [Question 15](#question-15) **


---

## Questions

### Question 1
###### What is the build number (in the format ddddd, where each d is a single decimal number, for example - 12345) of the installed Windows version?
> **Answer:** `17134`

Simple case of loading the image into `volatility` and running the `imageinfo` plugin.

![Windows Version](./images/01-Windows_Version.jpg)


### Question 2
###### What is the parent process PID of the process, that accepts incoming network connections on the port 1900/UDP?
> **Answer:** `648`

![Child PID](./images/02-Child_PID.jpg)
![Parent PID](./images/02-Parent_PID.jpg)

### Question 3
###### What is the IP address of the attacker command and control center, the connection with which was still active at the time of forensic artifacts acquisition?
> **Answer:** `196.6.112.70`

```
Image date and time : 2020-06-20 19:48:43 UTC+0000
Image local date and time : 2020-06-20 12:48:43 -0700
```

### Question 4
###### What is the PID of the process where malicious code was located at the moment of forensic artifacts acquisition?
> **Answer:** ``


### Question 5
###### On a compromised system, malicious code, discovered in the previous step, is launched every system start, since the attacker has used one of the persistence techniques. So, what is the name of the autostart entry (those part, that is directly responsible for code execution), used by the attacker for persistence?
> **Answer:** ``


### Question 6
###### The autostart entry from the previous step is used to launch the script, which in turn leads to the malicious code execution in the memory of the process, which is discussed in [question 4](#question-4). This code is extracted by script from some system place in the encoded form. The decoded value of this string is executable PE-file. How did Microsoft Antivirus detect this file on 2020-06-21?
> **Answer:** ``


### Question 7
###### The process, mentioned in the [question 4](#question-4), isn't the initial process, where malicious code, described in the previous question, was executed by script from autostart. What is the name of the initial process (in the format program.exe), that is spawned by autostart script and used for further malicious code execution, that subsequently migrates to the address space of the process, mentioned in the [question 4](#question-4)?
> **Answer:** ``


### Question 8
###### The autostart entry from the previous step is used to launch the script, which in turn leads to the malicious code execution in the memory of the process, which is discussed in question 4. Provide the URL, which was used to download this script from the Internet during the host compromise. The script that runs at each system star (which is described in [question 6](#question-6)) was downloaded to the compromised system from the Internet. Provide the URL, which was used to download this script
> **Answer:** ``


### Question 9
######
> **Answer:** ``


### Question 10
######
> **Answer:** ``


### Question 11
######
> **Answer:** ``


### Question 12
######
> **Answer:** ``


### Question 13
######
> **Answer:** ``


### Question 14
######
> **Answer:** ``


### Question 15
######
> **Answer:** ``
