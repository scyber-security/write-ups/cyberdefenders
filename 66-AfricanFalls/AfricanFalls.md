# [AfricanFalls](https://cyberdefenders.org/labs/66)

#### Scenario
John Doe was accused of doing illegal activities. A disk image of his laptop was taken. Your task is to analyze the image and understand what happened under the hood.

---
## Questions
### 1. What is the MD5 hash value of the suspect disk?
> `9471e69c95d8909ae60ddff30d50ffa1`

Can be found at the bottom of **DiskDrigger.ad1.txt** which accompanies the image.


### 2. What phrase did the suspect search for on 2021-04-29 18:17:38 UTC? (three words, two spaces in between)


### 3. What is the IPv4 address of the FTP server the suspect connected to?


### 4. What date and time was a password list deleted in UTC? (YYYY-MM-DD HH:MM:SS UTC)


### 5. How many times was Tor Browser ran on the suspect's computer? (number only)


### 6. What is the suspect's email address?


### 7. What is the FQDN did the suspect port scan?
> `dfir.science`


### 8. What country was picture "20210429_152043.jpg" allegedly taken in?


### 9. What is the parent folder name picture "20210429_151535.jpg" was in before the suspect copy it to "contact" folder on his desktop?


### 10. A Windows password hashes for an account are below. What is the user's password?
###### `Anon:1001:aad3b435b51404eeaad3b435b51404ee:3DE1A36F6DDB8E036DFD75E8E20C4AF4:::`


### 11. What is the user "John Doe's" Windows login password?
