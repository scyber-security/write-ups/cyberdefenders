# [Boss_Of_The_SOC_v2]()

#### Scenario


---

## Contents
- [Scenario](#scenario)
- [Questions](#questions)
    1. [Question 1](#question-1) *This is a simple question to get you familiar with submitting answers. What is the name of the company that makes the software that you are using for this competition?*
    2. [Question 2](#question-2) *Amber Turing was hoping for Frothly to be acquired by a potential competitor which fell through, but visited their website to find contact information for their executive team. What is the website domain that she visited?*
    3. [Question 3](#question-3) *Amber found the executive contact information and sent him an email. What is the CEO's name?*
    4. [Question 4](#question-4) *After the initial contact with the CEO, Amber contacted another employee at this competitor. What is that employee's email address?*
    5. [Question 5](#question-5) *What is the name of the file attachment that Amber sent to a contact at the competitor?*
    6. [Question 6](#question-6) *What is Amber's personal email address?*
    7. [Question 7](#question-7) *What version of TOR did Amber install to obfuscate her web browsing?*
    8. [Question 8](#question-8) *What is the public IPv4 address of the server running www.brewertalk.com?*
    9. [Question 9](#question-9) *Provide the IP address of the system used to run a web vulnerability scan against www.brewertalk.com.*
    10. [Question 10](#question-10) *The IP address from [question 9](#question-9) is also being used by a likely different piece of software to attack a URI path. What is the URI path?*
    11. [Question 11](#question-11) *What SQL function is being abused on the uri path from [question 10](#question-10)?*
    12. [Question 12](#question-12) *What is Frank Ester's password salt value on www.brewertalk.com?*
    13. [Question 13](#question-13) *What is user btun's password on brewertalk.com?*
    14. [Question 14](#question-14) *What are the characters displayed by the XSS probe?*
    15. [Question 15](#question-15) *What was the value of the cookie that Kevin's browser transmitted to the malicious URL as part of a XSS attack?*
    16. [Question 16](#question-16) *The brewertalk.com web site employed Cross Site Request Forgery (CSRF) techniques. What was the value of the anti-CSRF token that was stolen from Kevin Lagerfield's computer and used to help create an unauthorized admin user on brewertalk.com?*
    17. [Question 17](#question-17) *What brewertalk.com username was maliciously created by a spearphishing attack?*
    18. [Question 18](#question-18) *According to Frothly's records, what is the likely MAC address of Mallory's corporate MacBook?*
    19. [Question 19](#question-19) *What episode of Game of Thrones is Mallory excited to watch?*
    20. [Question 20](#question-20) *What is Mallory Krauesen's phone number?*
    21. [Question 21](#question-21) *Enterprise Security contains a threat list notable event for MACLORY-AIR13 and suspect IP address 5.39.93.112. What is the name of the threatlist (i.e. Threat Group) that is triggering the notable?*
    22. [Question 22](#question-22) *Considering the threatlist you found in the question above, and related data, what protocol often used for file transfer is actually responsible for the generated traffic?*
    23. [Question 23](#question-23) *Mallory's critical PowerPoint presentation on her MacBook gets encrypted by ransomware on August 18. At what hour, minute, and second does this actually happen?*
    24. [Question 24](#question-24) *How many seconds elapsed between the time the ransomware executable was written to disk on MACLORY-AIR13 and the first local file encryption?*
    25. [Question 25](#question-25) *Kevin Lagerfield used a USB drive to move malware onto kutekitten, Mallory's personal MacBook. She ran the malware, which obfuscates itself during execution. Provide the vendor name of the USB drive Kevin likely used.*
    26. [Question 26](#question-26) *What programming language is at least part of the malware from the question above written in?*
    27. [Question 27](#question-27) *The malware from the two questions above appears as a specific process name in the process table when it is running. What is it?*
    28. [Question 28](#question-28) *The malware infecting kutekitten uses dynamic DNS destinations to communicate with two C&C servers shortly after installation. What is the fully-qualified domain name (FQDN) of the first (alphabetically) of these destinations?*
    29. [Question 29](#question-29) *From the question above, what is the fully-qualified domain name (FQDN) of the second (alphabetically) contacted C&C server?*
    30. [Question 30](#question-30) *What is the average Alexa 1M rank of the domains between August 18 and August 19 that MACLORY-AIR13 tries to resolve while connected via VPN to the corporate network?*
    31. [Question 31](#question-31) *Two .jpg-formatted photos of Mallory exist in Kevin Lagerfield's server home directory that have eight-character file names, not counting the .jpg extension. Both photos were encrypted by the ransomware. One of the photos can be downloaded at the following link, replacing 8CHARACTERS with the eight characters from the file name. https://splunk.box.com/v/8CHARACTERS After you download the file to your computer, decrypt the file using the encryption key used by the ransomware. What is the complete line of text in the photo, including any punctuation?*
    32. [Question 32](#question-32) *A Federal law enforcement agency reports that Taedonggang often spearphishes its victims with zip files that have to be opened with a password. What is the name of the attachment sent to Frothly by a malicious Taedonggang actor?*
    33. [Question 33](#question-33) *The Taedonggang APT group encrypts most of their traffic with SSL. What is the "SSL Issuer" that they use for the majority of their traffic?*
    34. [Question 34](#question-34) *Threat indicators for a specific file triggered notable events on two distinct workstations. What IP address did both workstations have a connection with?*
    35. [Question 35](#question-35) *Based on the IP address found in question 34, what domain of interest is associated with that IP address?*
    36. [Question 36](#question-36) *What unusual file (for an American company) does winsys32.dll cause to be downloaded into the Frothly environment?*
    37. [Question 37](#question-37) *What is the first and last name of the poor innocent sap who was implicated in the metadata of the file that executed PowerShell Empire on the first victim's workstation?*
    38. [Question 38](#question-38) *What is the average Shannon entropy score of the subdomain containing UDP-exfiltrated data?*
    39. [Question 39](#question-39) *To maintain persistence in the Frothly network, Taedonggang APT configured several Scheduled Tasks to beacon back to their C2 server. What single webpage is most contacted by these Scheduled Tasks?*
    40. [Question 40](#question-40) *The APT group Taedonggang is always building more infrastructure to attack future victims. Provide the IPV4 IP address of a Taedonggang controlled server that has a completely different first octet to other Taedonggang controlled infrastructure.*
    41. [Question 41](#question-41) *The Taedonggang group had several issues exfiltrating data. Determine how many bytes were successfully transferred in their final, mostly successful attempt to exfiltrate files via a method using TCP, using only the data available in Splunk logs. Use 1024 for byte conversion.*
    42. [Question 42](#question-42) *Individual clicks made by a user when interacting with a website are associated with each other using session identifiers. You can find session identifiers in the stream:http sourcetype. The Frothly store website session identifier is found in one of the stream:http fields and does not change throughout the user session. What session identifier is assigned to dberry398@mail.com when visiting the Frothly store for the very first time?*
    43. [Question 43](#question-43) *How many unique user ids are associated with a grand total order of $1000 or more?*
    44. [Question 44](#question-44) *Which user, identified by their email address, edited their profile before placing an order over $1000 in the same clickstream?*
    45. [Question 45](#question-45) *What street address was used most often as the shipping address across multiple accounts, when the billing address does not match the shipping address?*
    46. [Question 46](#question-46) *What is the domain name used in email addresses by someone creating multiple accounts on the Frothly store website (http://store.froth.ly) that appear to have machine-generated usernames?*
    47. [Question 47](#question-47) *Which user ID experienced the most logins to their account from different IP address and user agent combinations?*
    48. [Question 48](#question-48) *What is the most popular coupon code being used successfully on the site?*
    49. [Question 49](#question-49) *Several user accounts sharing a common password is usually a precursor to undesirable scenario orchestrated by a fraudster. Which password is being seen most often across users logging into http://store.froth.ly ?*
    50. [Question 50](#question-50) *Which HTML page was most clicked by users before landing on http://store.froth.ly/magento2/checkout/ on August 19th?*
    51. [Question 51](#question-51) *Which HTTP user agent is associated with a fraudster who appears to be gaming the site by unsuccessfully testing multiple coupon codes?*


---

## Questions

### Question 1
###### This is a simple question to get you familiar with submitting answers. What is the name of the company that makes the software that you are using for this competition? Answer guidance: A six-letter word with no punctuation.
> **Answer:** `splunk`

*Simples*.


### Question 2
###### Amber Turing was hoping for Frothly to be acquired by a potential competitor which fell through, but visited their website to find contact information for their executive team. What is the website domain that she visited? Answer guidance: Do not provide the FQDN. Answer example: google.com
> **Search Term:** ``

> **Answer:** `berkbeer.com`


### Question 3
###### Amber found the executive contact information and sent him an email. What is the CEO's name? Provide the first and last name.
> **Search Term:** `source="stream:smtp" sender="mberk@berkbeer.com"`

> **Answer:** `Martin Berk`

My first search team was `source="stream:smtp" "\@berkbeer.com"` which gave me a few results. Once I was able to determine Amber's email address, I was able to narrow down the results using this search `source="stream:smtp" "\@berkbeer.com" sender_email="aturing@froth.ly"`. Which gave me two email addresses; the first being the answer to [question 4](#question-4), and the second being **mberk@berkbeer.com**.

![MBerk's Email Address](./images/03-mberk_email_address.png)

I then narrowed down emails to those which were sent by **mberk@berkbeer.com**, which thankfully was only one. However, searching for his name in all of the text was not easy.

![Martin Berk](./images/03-mberk_name.png)


### Question 4
###### After the initial contact with the CEO, Amber contacted another employee at this competitor. What is that employee's email address?
> **Search Term:** `source="stream:smtp" "\@berkbeer.com" sender_email="aturing@froth.ly"`

> **Answer:** `hbernhard@berkbeer.com`

Whilst looking for the answer to [question 3](#question-3), I stumbled across an email from *Heinz Bernhard* to *Amber* as the first result.

![Heinz's Email Address](./images/04-heinz_email_address.png)


### Question 5
###### What is the name of the file attachment that Amber sent to a contact at the competitor?
> **Search Term:** `source="stream:smtp""hbernhard@berkbeer.com"`

> **Answer:** `Saccharomyces_cerevisiae_patent.docx`

The first result which was returned was the last email that Amber sent to Heinz. Attached to this email was a word document. I had to expand the content to find it!

![Email Attachment - The hard way](./images/05-attachment_name.png)

Later I realised that I could just look at the interesting fields and see that the name of the attachment had already been picked out for me. *Lesson here is to take a quick look at the **interesting fields** section*.

![Email Attachment - The easy way](./images/05-attachment_name_alt.png)


### Question 6
###### What is Amber's personal email address?
> **Search Term:** `source="stream:smtp""hbernhard@berkbeer.com"`

> **Answer:** `ambersthebest@yeastiebeastie.com`

Looking at the same email from [question 5](#question-5), the **content_body** is encoded with *Base64*.

![Base64 Encoded](./images/06-encoded.png)

Using [CyberChef](https://gchq.github.io/CyberChef/), I was able to decode the message.

![Base64 Decoded](./images/06-decoded.png)


### Question 7
###### What version of TOR did Amber install to obfuscate her web browsing? Answer guidance: Numeric with one or more delimiter.
> **Search Term:** `tor install amber`

> **Answer:** `7.0.4`

A simple search term which returned the exact results I was looking for.

![Tor Version](./images/07-tor_version.png)


### Question 8
###### What is the public IPv4 address of the server running www.brewertalk.com?
> **Search Term:** `source="stream:dns" www.brewertalk.com`

> **Answer:** `52.42.208.228`

Another simple query looking at the DNS logs. The first result returned was incorrect as it's the location of the nameserver. The next result is the query to the nameserver for the `www`record.

![IP Address](./images/08-ip_address.png)


### Question 9
###### Provide the IP address of the system used to run a web vulnerability scan against www.brewertalk.com.
> **Search Term:** `www.brewertalk.com sourcetype="stream:http" | stats count by src_ip | sort - count`

> **Answer:** `45.77.65.211`

Typically, when a site is being scanned for vulnerabilities, the majority of the traffic logged will come from one IP address, and that's no different here.

![Scanner IP](./images/09-scan_ip.png)


### Question 10
###### The IP address from [question 9](#question-9) is also being used by a likely different piece of software to attack a URI path. What is the URI path? Answer guidance: Include the leading forward slash in your answer. Do not include the query string or other parts of the URI. Answer example: /phpinfo.php
> **Search Term:** `www.brewertalk.com sourcetype="stream:http" src_ip="45.77.65.211" | stats count by uri_path | sort - count`

> **Answer:** `/member.php`

Using the IP address gain through [question 9](#question-9), I counted the number of occurances the different *URI paths* occurred within the dataset. It's likely `/member.php` is the path being abused as it occurs the most number of times.

![URI Path](./images/10-uri_path.png)


### Question 11
###### What SQL function is being abused on the uri path from [question 10](#question-10)?
> **Search Term:** `www.brewertalk.com sourcetype="stream:http" src_ip="45.77.65.211" uri_path="/member.php"`

> **Answer:** `updatexml`

Using the search time, I was able to limit the results to just the URI path obtained in [question 10](#question-10). The first result gave the answer, but it was buried amongst the *form_data* of the *POST* request.

![Form Data](./images/11-sql_function.png)


### Question 12
###### What is Frank Ester's password salt value on www.brewertalk.com?
> **Search Term:** `sourcetype="stream:http" src_ip="45.77.65.211" "SELECT salt"`

> **Answer:** `gGsxysZL`

The first search I performed was using `sourcetype="stream:http" src_ip="45.77.65.211" frank*`. This returned two results, one of which was Frank's email address within some `XPATH` error.

![Frank's Email](./images/12-frank_email.png)

The next part took me some time to work out, so much so that I needed to use some hints. I then set the time frame to within +/- 5 seconds of the event.

![Time frame](./images/12-timeframe.png)

Once the time frame was set, I needed to change the search term to look for the password salt instead `sourcetype="stream:http" src_ip="45.77.65.211" "SELECT salt"`. Only three results were returned, of which the top one gave me the salt.

![salt](./images/12-salt.png)


### Question 13
###### What is user btun's password on brewertalk.com?
> **Search Term:** `sourcetype="stream:http" src_ip="45.77.65.211" "SELECT salt"`

> **Answer:** ``

The steps to answering this question are similar to [question 12](#question-12). First I needed to find the rough time of when the attacker was looking for anything to do with the *btun* user: `sourcetype="stream:http" src_ip="45.77.65.211" btun*`. This search string returned two results, one of which is *btun*'s email address.

![btun's email address](./images/13-btun_email.png)

With the timeframe locked into +/- 5 seconds from the result with the email address, I ran the search `sourcetype="stream:http" src_ip="45.77.65.211" "SELECT salt"` to get the results where the attacker was looking for the salt. The first result in the list, returned the salt for the *btun* user: **tlX7cQPE**

![btun's password salt](./images/13-btun_salt.png)

Once I had found the salt, I changed the query to look for password instead: `sourcetype="stream:http" src_ip="45.77.65.211" "SELECT password"`. The third result down from the top gave me the user's password: **f91904c1dd2723d5911eeba409cc0d1**

![btun's password hash](./images/13-btun_password.png)

I loaded up `hash-id` and tried to work through various combinations of the salt and hash to try to discover what kind of hash I was dealing with, but unfortunately, none of them were identified.

![Failed hash indentity](./images/13-hash_id.png)


### Question 14
###### What are the characters displayed by the XSS probe? Answer guidance: Submit answer in native language or character set.
> **Search Term:** `sourcetype="stream:http" "<script>" | dedup form_data"`

> **Answer:** `대동`

My first attempt at looking for the language was to use the search `sourcetype="stream:http" src_ip="45.77.65.211" language`. This unfortunately didn't return anything of interest.

Next up I tried to look for the opening of a script tag using the search `sourcetype="stream:http" "<script>"` which returned quite a few results, over 1,900; too many to sift through by hand.

Adding the `dedup` command reduced the number of results down to just 3; a little more manageable. Scrolling through the results, I noticed an interesting script object placed into a cell of a table.

![Alert XSS](./images/14-alert.png)


### Question 15
###### What was the value of the cookie that Kevin's browser transmitted to the malicious URL as part of a XSS attack? Answer guidance: All digits. Not the cookie name or symbols like an equal sign.
> **Search Term:** `sourcetype="stream:http" "<script>" kevin`

> **Answer:** `1502408189`


### Question 16
###### The brewertalk.com web site employed Cross Site Request Forgery (CSRF) techniques. What was the value of the anti-CSRF token that was stolen from Kevin Lagerfield's computer and used to help create an unauthorized admin user on brewertalk.com?


### Question 17
###### What brewertalk.com username was maliciously created by a spearphishing attack?


### Question 18
###### According to Frothly's records, what is the likely MAC address of Mallory's corporate MacBook? Answer guidance: Her corporate MacBook has the hostname MACLORY-AIR13.


### Question 19
###### What episode of Game of Thrones is Mallory excited to watch? Answer guidance: Submit the HBO title of the episode.


### Question 20
###### What is Mallory Krauesen's phone number? Answer guidance: ddd-ddd-dddd where d=[0-9]. No country code.


### Question 21
###### Enterprise Security contains a threat list notable event for MACLORY-AIR13 and suspect IP address 5.39.93.112. What is the name of the threatlist (i.e. Threat Group) that is triggering the notable?


### Question 22
###### Considering the threatlist you found in the question above, and related data, what protocol often used for file transfer is actually responsible for the generated traffic?


### Question 23
###### Mallory's critical PowerPoint presentation on her MacBook gets encrypted by ransomware on August 18. At what hour, minute, and second does this actually happen? Answer guidance: Provide the time in PDT. Use the 24h format HH:MM:SS, using leading zeroes if needed. Do not use Splunk's _time (index time).


### Question 24
###### How many seconds elapsed between the time the ransomware executable was written to disk on MACLORY-AIR13 and the first local file encryption? Answer guidance: Use the index times (_time) instead of other timestamps in the events.


### Question 25
###### Kevin Lagerfield used a USB drive to move malware onto kutekitten, Mallory's personal MacBook. She ran the malware, which obfuscates itself during execution. Provide the vendor name of the USB drive Kevin likely used. Answer Guidance: Use time correlation to identify the USB drive.


### Question 26
###### What programming language is at least part of the malware from the question above written in?


### Question 27
###### The malware from the two questions above appears as a specific process name in the process table when it is running. What is it?


### Question 28
###### The malware infecting kutekitten uses dynamic DNS destinations to communicate with two C&C servers shortly after installation. What is the fully-qualified domain name (FQDN) of the first (alphabetically) of these destinations?


### Question 29
###### From the question above, what is the fully-qualified domain name (FQDN) of the second (alphabetically) contacted C&C server?


### Question 30
###### What is the average Alexa 1M rank of the domains between August 18 and August 19 that MACLORY-AIR13 tries to resolve while connected via VPN to the corporate network? Answer guidance: Round to two decimal places. Remember to include domains with no rank in your average! Answer example: 3.23 or 223234.91


### Question 31
###### Two .jpg-formatted photos of Mallory exist in Kevin Lagerfield's server home directory that have eight-character file names, not counting the .jpg extension. Both photos were encrypted by the ransomware. One of the photos can be downloaded at the following link, replacing 8CHARACTERS with the eight characters from the file name. https://splunk.box.com/v/8CHARACTERS After you download the file to your computer, decrypt the file using the encryption key used by the ransomware. What is the complete line of text in the photo, including any punctuation? Answer guidance: The encryption key can be found in Splunk.


### Question 32
###### A Federal law enforcement agency reports that Taedonggang often spearphishes its victims with zip files that have to be opened with a password. What is the name of the attachment sent to Frothly by a malicious Taedonggang actor?


### Question 33
###### The Taedonggang APT group encrypts most of their traffic with SSL. What is the "SSL Issuer" that they use for the majority of their traffic? Answer guidance: Copy the field exactly, including spaces.


### Question 34
###### Threat indicators for a specific file triggered notable events on two distinct workstations. What IP address did both workstations have a connection with?


### Question 35
###### Based on the IP address found in question 34, what domain of interest is associated with that IP address?


### Question 36
###### What unusual file (for an American company) does winsys32.dll cause to be downloaded into the Frothly environment?


### Question 37
###### What is the first and last name of the poor innocent sap who was implicated in the metadata of the file that executed PowerShell Empire on the first victim's workstation? Answer example: John Smith


### Question 38
###### What is the average Shannon entropy score of the subdomain containing UDP-exfiltrated data? Answer guidance: Cut off, not rounded, to the first decimal place. Answer examples: 3.2 or 223234.9


### Question 39
###### To maintain persistence in the Frothly network, Taedonggang APT configured several Scheduled Tasks to beacon back to their C2 server. What single webpage is most contacted by these Scheduled Tasks? Answer guidance: Remove the path and type a single value with an extension. Answer example: index.php or images.html


### Question 40
###### The APT group Taedonggang is always building more infrastructure to attack future victims. Provide the IPV4 IP address of a Taedonggang controlled server that has a completely different first octet to other Taedonggang controlled infrastructure. Answer guidance: 4.4.4.4 has a different first octet than 8.4.4.4


### Question 41
###### The Taedonggang group had several issues exfiltrating data. Determine how many bytes were successfully transferred in their final, mostly successful attempt to exfiltrate files via a method using TCP, using only the data available in Splunk logs. Use 1024 for byte conversion.


### Question 42
###### Individual clicks made by a user when interacting with a website are associated with each other using session identifiers. You can find session identifiers in the stream:http sourcetype. The Frothly store website session identifier is found in one of the stream:http fields and does not change throughout the user session. What session identifier is assigned to dberry398@mail.com when visiting the Frothly store for the very first time? Answer guidance: Provide the value of the field, not the field name.


### Question 43
###### How many unique user ids are associated with a grand total order of $1000 or more?


### Question 44
###### Which user, identified by their email address, edited their profile before placing an order over $1000 in the same clickstream? Answer guidance: Provide the user ID, not other values found from the profile edit, such as name.


### Question 45
###### What street address was used most often as the shipping address across multiple accounts, when the billing address does not match the shipping address? Answer example: 123 Sesame St


### Question 46
###### What is the domain name used in email addresses by someone creating multiple accounts on the Frothly store website (http://store.froth.ly) that appear to have machine-generated usernames?


### Question 47
###### Which user ID experienced the most logins to their account from different IP address and user agent combinations? Answer guidance: The user ID is an email address.


### Question 48
###### What is the most popular coupon code being used successfully on the site?


### Question 49
###### Several user accounts sharing a common password is usually a precursor to undesirable scenario orchestrated by a fraudster. Which password is being seen most often across users logging into http://store.froth.ly?


### Question 50
###### Which HTML page was most clicked by users before landing on http://store.froth.ly/magento2/checkout/ on August 19th? Answer guidance: Use earliest=1503126000 and latest=1503212400 to identify August 19th. Answer example: http://store.froth.ly/magento2/bigbrew.html


### Question 51
###### Which HTTP user agent is associated with a fraudster who appears to be gaming the site by unsuccessfully testing multiple coupon codes?
