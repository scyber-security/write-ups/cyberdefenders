# [WireDive](https://cyberdefenders.org/labs/37)


#### Scenario
WireDive is a combo traffic analysis exercise that contains various traces to help you understand how different protocols look on the wire.


---
## Questions

### 1. *File: dhcp.pcapng* - What IP address is requested by the client?
>
