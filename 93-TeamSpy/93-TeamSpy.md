# [TeamSpy](https://cyberdefenders.org/blueteam-ctf-challenges/93)


#### Scenario


---

## Contents
- [Scenario](#scenario)
- [Questions](#questions)
    1. [Question 1](#question-1) *__File->ecorpoffice__ - What is the PID the malicious file is running under?*
    2. [Question 2](#question-2) *__File->ecorpoffice__ - What is the C2 server IP address?*
    3. [Question 3](#question-3) *__File->ecorpoffice__ - What is the Teamviewer version abused by the malicious file?*
    4. [Question 4](#question-4) *__File->ecorpoffice__ - What password did the malicious file use to enable remote access to the system?*
    5. [Question 5](#question-5) *__File->ecorpoffice__ - What was the sender's email address that delivered the phishing email?*
    6. [Question 6](#question-6) *__File->ecorpoffice__ - What is the MD5 hash of the malicious document?*
    7. [Question 7](#question-7) *__File->ecorpoffice__ - What is the bitcoin wallet address that ransomware was demanded?*
    8. [Question 8](#question-8) *__File->ecorpoffice__ - What is the ID given to the system by the malicious file for remote access?*
    9. [Question 9](#question-9) *__File->ecorpoffice__ - What is the IPv4 address the actor last connected to the system with the remote access tool?*
    10. [Question 10](#question-10) *__File->ecorpoffice__ - What Public Function in the word document returns the full command string that is eventually run on the system?*
    11. [Question 11](#question-11) **
    12. [Question 12](#question-12) **
    13. [Question 13](#question-13) **
    14. [Question 14](#question-14) **
    15. [Question 15](#question-15) **
    16. [Question 16](#question-16) **


---

## Questions

### Question 1
###### __File->ecorpoffice__ - What is the PID the malicious file is running under?
> **Answer:** `1364`

```
volatility.exe -f .\win7ecorpoffice2010-36b02ed3.vmem imageinfo
```

```
volatility.exe -f .\win7ecorpoffice2010-36b02ed3.vmem --profile Win7SP1x64 pstree
```


### Question 2
###### __File->ecorpoffice__ - What is the C2 server IP address?
> **Answer:** `54.174.131.235`

```PowerShell
volatility.exe -f .\win7ecorpoffice2010-36b02ed3.vmem --profile Win7SP1x64 netscan | Select-String -Pattern "1364"
```


### Question 3
###### __File->ecorpoffice__ - What is the Teamviewer version abused by the malicious file?
> **Answer:** `0.2.2.2`

```
volatility.exe -f .\win7ecorpoffice2010-36b02ed3.vmem --profile Win7SP1x64 procdump -p 1364 --dump-dir .
```


### Question 4
###### __File->ecorpoffice__ - What password did the malicious file use to enable remote access to the system?
> **Answer:** `P59fS93m`

```
vol2 -f win7ecorpoffice2010-36b02ed3.vmem --profile Win7SP1x64 editbox
```


### Question 5
###### __File->ecorpoffice__ - What was the sender's email address that delivered the phishing email?
> **Answer:** ``


### Question 6
###### __File->ecorpoffice__ - What is the MD5 hash of the malicious document?
> **Answer:** ``


### Question 7
###### __File->ecorpoffice__ - What is the bitcoin wallet address that ransomware was demanded?
> **Answer:** ``


### Question 8
###### __File->ecorpoffice__ - What is the ID given to the system by the malicious file for remote access?
> **Answer:** ``


### Question 9
###### __File->ecorpoffice__ - What is the IPv4 address the actor last connected to the system with the remote access tool?
> **Answer:** ``


### Question 10
###### __File->ecorpoffice__ - What Public Function in the word document returns the full command string that is eventually run on the system?
> **Answer:** ``


### Question 11
######
> **Answer:** ``


### Question 12
######
> **Answer:** ``


### Question 13
######
> **Answer:** ``


### Question 14
######
> **Answer:** ``


### Question 15
######
> **Answer:** ``


### Question 16
######
> **Answer:** ``
