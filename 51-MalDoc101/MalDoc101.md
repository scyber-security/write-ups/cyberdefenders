# [MalDoc101](https://cyberdefenders.org/labs/51)


#### Scenario
It is common for threat actors to utilize living off the land (LOTL) techniques, such as the execution of PowerShell to further their attacks and transition from macro code. This challenge is intended to show how you can often times perform quick analysis to extract important IOCs. The focus of this exercise is on static techniques for analysis.

---

## Contents
- [Scenario](#scenario)
- [Resources](#resources)
- [Questions](#questions)
    1. [Question 1](#question-1) *Multiple streams contain macros in this document. Provide the number of highest one.*
    2. [Question 2](#question-2) *What event is used to begin the execution of the macros?*
    3. [Question 3](#question-3) *What malware family was this maldoc attempting to drop?*
    4. [Question 4](#question-4) *What stream is responsible for the storage of the base64-encoded string?*
    5. [Question 5](#question-5) *This document contains a user-form. Provide the name?*
    6. [Question 6](#question-6) *This document contains an obfuscated base64 encoded string; what value is used to pad (or obfuscate) this string?*
    7. [Question 7](#question-7) *What is the program executed by the base64 encoded string?*
    8. [Question 8](#question-8) *What WMI class is used to create the process to launch the trojan?*
    9. [Question 9](#question-9) *Multiple domains were contacted to download a trojan. Provide first FQDN as per the provided hint.*

---

## Resources

---

## Questions

### Question 1
#### Multiple streams contain macros in this document. Provide the number of highest one.
> **Answer:** `16`

```
oldump.py sample.bin
```

![](./images/01-macros.png)


### Question 2
#### What event is used to begin the execution of the macros?
> **Answer:** `Document_open`

```
oledump.py -vs13 sample.bin
```

![](./images/02-event.png)


### Question 3
#### What malware family was this maldoc attempting to drop?
> **Answer:** `Emotet`

Uploaded the sample to [Hybrid Analysis](https://www.hybrid-analysis.com/sample/d50d98dcc8b7043cb5c38c3de36a2ad62b293704e3cf23b0cd7450174df53fee) which identified it as **Emotet**.

Can also run `clamscan` to identify the sample.

```bash
freshclam
clamscan sample.bin
```

![](./images/03-clamscan.png)


### Question 4
#### What stream is responsible for the storage of the base64-encoded string?
> **Answer:** `34`

```
oledump.py sample.bin -ds34
```

A bit of trial and error with this. Iterated over all until I came across something which looked like a Base64 encoded string.


### Question 5
#### This document contains a user-form. Provide the name?
> **Answer:** `roubhaol`

Opened up the sample in *LibreOffice Writer* and viewed the macros.

![](./images/05-forms.png)


### Question 6
#### This document contains an obfuscated base64 encoded string; what value is used to pad (or obfuscate) this string?
> **Answer:** `2342772g3&*gs7712ffvs626fq`

There is a variable is the 15th by the name of `haothkoebtheil`. This value is passed into the function `juuvzouchmiopxeox`. Within this function the string is split on the answer.

![](./images/06.1-string.png)
![](./images/06.2-split.png)


### Question 7
#### What is the program executed by the base64 encoded string?
> **Answer:** `powershell`

This was a tricky one. The answer was obvious based on the strings found when looking on VirusTotal. However, if we take the strings from the stream identified in [Question 4](#question-4), and split it based on the string found in [Question 6](#question-6).

![](./images/07-powershell.png)


### Question 8
#### What WMI class is used to create the process to launch the trojan?
> **Answer:** `win32_Process`

Decode the Base-64 encoded string of the PowerShell command to get the WMI Class.

![](./images/08-09-Base_64.png)


### Question 9
#### Multiple domains were contacted to download a trojan. Provide first FQDN as per the provided hint.
> **Answer:** `haoqunkong.com`

Can be found when looking at the decoded base-64.
