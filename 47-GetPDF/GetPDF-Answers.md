# [GetPDF](https://cyberdefenders.org/blueteam-ctf-challenges/47)

## Scenario
PDF format is the de-facto standard in exchanging documents online. Such popularity, however, has also attracted cyber criminals in spreading malware to unsuspecting users. The ability to generate malicious pdf files to distribute malware is a functionality that has been built into many exploit kits. As users are less cautious about opening PDF files, the malicious PDF file has become quite a successful attack vector. The network traffic is captured in lala.pcap contains network traffic related to a typical malicious PDF file attack, in which an unsuspecting user opens a compromised web page, which redirects the user’s web browser to a URL of a malicious PDF file. As the PDF plug-in of the browser opens the PDF, the unpatched version of Adobe Acrobat Reader is exploited and, as a result, downloads and silently installs malware on the user’s machine.

---
## Contents
- [Scenario](#scenario)
- [Questions](#questions)
    1. [Question 1](#question-1) *How many URL path(s) are involved in this incident?*
    2. [Question 2](#question-2) *What is the URL which contains the JS code?*
    3. [Question 3](#question-3) *What is the URL hidden in the JS code?*
    4. [Question 4](#question-4) *What is the MD5 hash of the PDF file contained in the packet?*
    5. [Question 5](#question-5) *How many object(s) are contained inside the PDF file?*
    6. [Question 6](#question-6) *How many filtering schemes are used for the object streams?*
    7. [Question 7](#question-7) *What is the number of the 'object stream' that might contain malicious JS code?*
    8. [Question 8](#question-8) *Analyzing the PDF file. What 'object-streams' contain the JS code responsible for executing the shellcodes? The JS code is divided into two streams. Format: two numbers separated with ','. Put the numbers in ascending order*
    9. [Question 9](#question-9) *The JS code responsible for executing the exploit contains shellcodes that drop malicious executable files. What is the full path of malicious executable files after being dropped by the malware on the victim machine?*
    10. [Question 10](#question-10) *The PDF file contains another exploit related to CVE-2010-0188. What is the URL of the malicious executable that the shellcode associated with this exploit drop?*
    11. [Question 11](#question-11) *How many CVEs are included in the PDF file?*


---

## Questions
### Question 1
###### How many URL path(s) are involved in this incident?
> **Answer:** `6`


### Question 2
###### What is the URL which contains the JS code?
> **Answer:** `ttp://blog.honeynet.org.my/forensic_challenge/`


### Question 3
###### What is the URL hidden in the JS code?
> **Answer:** `http://blog.honeynet.org.my/forensic_challenge/getpdf.php`


### Question 4
###### What is the MD5 hash of the PDF file contained in the packet?
> **Answer:** `659cf4c6baa87b082227540047538c2a`


### Question 5
###### How many object(s) are contained inside the PDF file?
> **Answer:** `19`


### Question 6
###### How many filtering schemes are used for the object streams?
> **Answer:** `4`


### Question 7 What is the number of the 'object stream' that might contain malicious JS code?
###### How many URL path(s) are involved in this incident?
> **Answer:** ``


### Question 8
###### Analyzing the PDF file. What 'object-streams' contain the JS code responsible for executing the shellcodes? The JS code is divided into two streams. Format: two numbers separated with ','. Put the numbers in ascending order
> **Answer:** ``


### Question 9
###### The JS code responsible for executing the exploit contains shellcodes that drop malicious executable files. What is the full path of malicious executable files after being dropped by the malware on the victim machine?
> **Answer:** ``


### Question 10
###### The PDF file contains another exploit related to CVE-2010-0188. What is the URL of the malicious executable that the shellcode associated with this exploit drop?
> **Answer:** `http://blog.honeynet.org.my/forensic_challenge/the_real_malware.exe`


### Question 11
###### How many CVEs are included in the PDF file?
> **Answer:** ``
