# [PacketMaze](https://cyberdefenders.org/labs/68)

#### Scenario
As an analyst working for a security service provider, you have been tasked with analyzing a packet capture for a customer's employee whose network activity has been monitored for a while -possible insider.


---
## Questions
### 1. What is the FTP password?
> `AfricaCTF2021`

**Filter:** `ftp`

Using the above filter, I could filter out all but the FTP traffic. The FTP password was quite obvious at this point; looking at the **PASS** request.

![FTP Password](./images/01-ftp_password.png)


### 2. What is the IPv6 address of the DNS server used by 192.168.1.26? (####::####:####:####:####)
> `fe80::c80b:adff:feaa:1db7`

**Filter 1:** `dns && ip.src_host == 192.168.1.26`

**Filter 2:** `dns && eth.addr == c8:09:a8:57:47:93`

This question required two filters to get the answer. The first step was to get the **MAC** address of the interface sending the requests.

![MAC Address](./images/02-mac.png)

Once I had the **MAC** address, I could swap out the IPv4 address filter with the filter for the MAC address for the NIC.

![IPv6](./images/02-ipv6.png)


### 3. What domain is the user looking up in packet 15174?
> `www.7-zip.org`

**Filter:** `frame.number == 15174`

Not much to say here...

![DNS Lookup](./images/03-lookup.png)


### 4. How many UDP packets were sent from 192.168.1.26 to 24.39.217.246?
> `10`

**Filter:** `udp && ip.src==192.168.1.26 && ip.dst == 24.39.217.246`

![Number of Packets](./images/04-num_packets.png)


### 5. What is the MAC address of the system being monitored?
> `c8:09:a8:57:47:93`

This was a guess based on the questions asked beforehand. The answer came from the first part of the first question.


### 6. What was the time when the picture 20210429_152157.jpg was taken on the 29th of April? (hh:mm:ss)
> `15:21:57`

My initial thought was to try to export the file, and get some stats off of it. Then I realised that the timestamp was included in the file name!


### 7. What is the server certificate public key that was used in TLS session: da4a0000342e4b73459d7360b4bea971cc303ac18d29b99067e46d16cc07f4ff?
> `04edcc123af7b13e90ce101a31c2f996f471a7c8f48a1b81d765085f548059a550f3f4f62ca1f0e8f74d727053074a37bceb2cbdc7ce2a8994dcd76dd6834eefc5438c3b6da929321f3a1366bd14c877cc83e5d0731b7f80a6b80916efd4a23a4d`

**Filter:** `tls.handshake.session_id == da:4a:00:00:34:2e:4b:73:45:9d:73:60:b4:be:a9:71:cc:30:3a:c1:8d:29:b9:90:67:e4:6d:16:cc:07:f4:ff`

I started off by filtering only **TLS** packets, and then added the filter `tls.handshake.type == 2`; *Server Hello* packets where key exchanges are done. I looked for a packet where a *session_id* was set and filtered out by that. Taking the session ID from the question I substituted the value adding `:` every second character, which gave me only one packet.

![Public Key](./images/07-public_key.png)


### 8. What is the first TLS 1.3 client random that was used to establish a connection with protonmail.com?
> `24e92513b97a0348f733d16996929a79be21b0b1400cd7e2862a732ce7775b70`

**Filter 1:** `dns.qry.name == "protonmail.com"`

**Filter 2:** `ip.dst== 185.70.41.35`

Initially I tried to search for `http.host == "protonmail.com"`, but this produced no packets. Which now that I think about it isn't all that surprising.

The hostname would have had to have been looked up, so I did a quick filter of the DNS packets looking for only **protonmail.com** which gave me the IP address that would have been used. Once I had that, I filtered packets by that IP, and one of the first few packets provided me with the answer.

![Secret](./images/08-random_id.png)


### 9. What country is the MAC address of the FTP server registered in? (two words, one space in between)
> `United States`

Filtering by `ftp`, I could see all of the FTP packets. The IP address of the server is `192.168.1.26` which yielded the MAC address of `08:00:27:a6:1f:86`. I did a quick search using the following site https://macvendorlookup.com/ and got the location of the where the MAC address was registered.


### 10. What time was a non-standard folder created on the FTP server on the 20th of April? (hh:mm)
> `17:53`

**Filter:** `ftp-data`

Initially I filtered all traffic so that only FTP traffic was shown, but this unfortunately didn't return much results. Interestingly there was a **LIST** command within the results but no data.

I took a closer look at the packet capture and noticed that there was another protocol **ftp-data**. After filtering out everything but this protocol, the first packet in the results was that of the **LIST** data. Out of the directories which were listed, only the **ftp** directory is not part of the standard directories in a user's home directory.

![FTP Directory](./images/10-ftp.png)


### 11. What domain was the user connected to in packet 27300?
> `dfir.science`

**Filter 1:** `frame.number == 27300`

**Filter 2:** `dns.a == 172.67.162.206`

The first filter picks out the individual frame which was specified the question. This gave me the IP address of the server **172.67.162.206**. The next step was to get the DNS A record which would have been returned following on from a DNS lookup.

![Address Book](./images/11-domain.png)
