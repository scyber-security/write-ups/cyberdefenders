# [Hammered](https://cyberdefenders.org/labs/42)


#### Scenario
This challenge takes you into the world of virtual systems and confusing log data. In this challenge, figure out what happened to this webserver honeypot using the logs from a possibly compromised server.

---
## Questions
### 1. Which service did the attackers use to gain access to the system?
> `ssh`

Initially a guess as the hint had three characters! But, taking a look at the **auth.log** files, there are a number of failed attemps where it looks like someone is trying to break into the system.

![Failed SSH](./images/01-Failed_Attempts.png)


### 2. What is the operating system version of the targeted system? (one word)
> `4.2.4-1ubuntu3`

This is in the **kern.log** file, and can be found by using the `head` command.


### 3. What is the name of the compromised account?
> `root`

This was another guess based on the number of characters hinted in the text field. The evidence of this can be found in the **auth.log** file where it looks like there was an entry for `Accepted password for root`.

![I am Root](./images/03-root.png)


### 4. Question Missing!

### 5. Consider that each unique IP represents a different attacker. How many attackers were able to get access to the system?


### 6. Which attacker's IP address successfully logged into the system the most number of times?
> `6`


### 7. How many requests were sent to the Apache Server?
> `365`

This was a simple case of running `wc -l www-access.log` within the **apache2** directory.


### 8. How many rules have been added to the firewall?
> `6`

The first thing I needed to work out, was which of the many utilities were they using for their firewall. My first search was `grep -i firewall auth.log`, which returned no results. Next was `grep -i iptables auth.log`, this returned some results.

Refining the command based on the output, I came up with this: `grep "iptables -A" auth.log | wc -l`

![Firewall rulez](./images/08-firewall.png)


### 9. One of the downloaded files to the target system is a scanning tool. Provide the tool name.
> `nmap`

Within the **apt** directory is log file by the name of **term.log**. Running the command `grep Unpacking term.log` I was able to get a list of packages which had been installed on the system. At the bottom of this was `nmap`, a very common network scanning tool.


### 10. When was the last login from the attacker with IP 219.150.161.20? Format: MM/DD/YYYY HH:MM:SS AM


`grep "219.150.161.20" auth.log | grep -i accepted`


### 11. The database displayed two warning messages, provide the most important and dangerous one.
> `mysql.user contains 2 root accounts without password!`

The events for the MySQL are logged into **daemon.log**. The command `grep -i mysql daemon.log | grep -i warning` displays only the MySQL logs which contain the word *warning*.

![MySQL oopsie](./images/11-mysql.png)


### 12. Multiple accounts were created on the target system. Which one was created on Apr 26 04:43:15?
> `wind3str0y`

This was a simple command to get to this answer `grep "Apr 26 04:43:15" auth.log`.

![d3str0yer of win](./images/12-new_account.png)

### 13. Few attackers were using a proxy to run their scans. What is the corresponding user-agent used by this proxy?
