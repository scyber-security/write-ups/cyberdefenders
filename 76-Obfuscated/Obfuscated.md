# [Obfuscated](https://cyberdefenders.org/blueteam-ctf-challenges/76)


#### Scenario
During your shift as a SOC analyst, the enterprise EDR alerted a suspicious behavior from an end-user machine. The user indicated that he received a recent email with a DOC file from an unknown sender and passed the document for you to analyze.

---

## Contents
- [Scenario](#scenario)
- [Questions](#questions)
    1. [Question 1](#question-1) *What is the sha256 hash of the doc file?*
    2. [Question 2](#question-2) *Multiple streams contain macros in this document. Provide the number of lowest one.*
    3. [Question 3](#question-3) *What is the decryption key of the obfuscated code?*
    4. [Question 4](#question-4) *What is the name of the dropped file?*
    5. [Question 5](#question-5) *This script uses what language?*
    6. [Question 6](#question-6) *What is the name of the variable that is assigned the command-line arguments?*
    7. [Question 7](#question-7) *How many command-line arguments does this script expect?*
    8. [Question 8](#question-8) *What instruction is executed if this script encounters an error?*
    9. [Question 9](#question-9) *What function returns the next stage of code (i.e. the first round of obfuscated code)?*
    10. [Question 10](#question-10) *The function LXv5 is an important function, what variable is assigned a key string value in determining what this function does?*
    11. [Question 11](#question-11) *What encoding scheme is this function responsible for decoding?*
    12. [Question 12](#question-12) *In the function CpPT, the first two for loops are responsible for what important part of this function?*
    13. [Question 13](#question-13) *The function CpPT requires two arguments, where does the value of the first argument come from?*
    14. [Question 14](#question-14) *For the function CpPT, what does the first argument represent?*
    15. [Question 15](#question-15) *What encryption algorithm does the function CpPT implement in this script?*
    16. [Question 16](#question-16) *What function is responsible for executing the deobfuscated code?*
    17. [Question 17](#question-17) *What Windows Script Host program can be used to execute this script in command-line mode?*
    18. [Question 18](#question-18) *What is the name of the first function defined in the deobfuscated code?*

---

## Resources
- https://app.any.run/tasks/5840a1fa-9ad9-4c9e-963a-7bc5b6fa3eff/
- https://blog.malwarebytes.com/cybercrime/2016/02/de-obfuscating-malicious-vbscripts/
- https://www.geeksforgeeks.org/rc4-encryption-algorithm/

---

## Questions

### Question 1
###### What is the sha256 hash of the doc file?
> **Answer:** `ff2c8cadaa0fd8da6138cce6fce37e001f53a5d9ceccd67945b15ae273f4d751`


### Question 2
###### Multiple streams contain macros in this document. Provide the number of lowest one.
> **Answer:** `8`


### Question 3
###### What is the decryption key of the obfuscated code?
> **Answer:** `EzZETcSXyKAdF_e5I2i1`


### Question 4
###### What is the name of the dropped file?
> **Answer:** `maintools.js`


### Question 5
###### This script uses what language?
> **Answer:** `jscript`


### Question 6
###### What is the name of the variable that is assigned the command-line arguments?
> **Answer:** `wvy1`


### Question 7
###### How many command-line arguments does this script expect?
> **Answer:** `1`


### Question 8
###### What instruction is executed if this script encounters an error?
> **Answer:** `WScript.Quit()`


### Question 9
###### What function returns the next stage of code (i.e. the first round of obfuscated code)?
> **Answer:** `y3zb`


### Question 10
###### The function LXv5 is an important function, what variable is assigned a key string value in determining what this function does?
> **Answer:** `LUK7`


### Question 11
###### What encoding scheme is this function responsible for decoding?
> **Answer:** `Base64`


### Question 12
###### In the function CpPT, the first two for loops are responsible for what important part of this function?
> **Answer:** `Key-Scheduling Algorithm`


### Question 13
###### The function CpPT requires two arguments, where does the value of the first argument come from?
> **Answer:** `Key-Scheduling Algorithm`


### Question 14
###### For the function CpPT, what does the first argument represent?
> **Answer:** `key`


### Question 15
###### What encryption algorithm does the function CpPT implement in this script?
> **Answer:** `RC4`


### Question 16
###### What function is responsible for executing the deobfuscated code?
> **Answer:** `eval`


### Question 17
###### What Windows Script Host program can be used to execute this script in command-line mode?
> **Answer:** `cscript.exe`


### Question 18
###### What is the name of the first function defined in the deobfuscated code?
> **Answer:** `UspD`

Use CyberChef to decode and decrypt
