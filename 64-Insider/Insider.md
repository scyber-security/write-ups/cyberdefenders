# [Inside](https://cyberdefenders.org/labs/64)


#### Scenario
After Karen started working for 'TAAUSAI,' she began to do some illegal activities inside the company. 'TAAUSAI' hired you to kick off an investigation on this case.

You acquired a disk image and found that Karen uses Linux OS on her machine. Analyze the disk image of Karen's computer and answer the provided questions.

---
## Questions

### 1. What distribution of Linux is being used on this machine?
> kali

The directories available to work with are a little limited. This information could normally be found in files such as **/etc/os-release**. There was however a **/boot** directory with a *GRUB* configuration file. After a little scrolling, I found what I was looking for:

![Kali - Brub](./images/01-Kali.png)


### 2. What is the MD5 hash of the apache access.log?
> d41d8cd98f00b204e9800998ecf8427e

This was a simple case of extracting the file **/var/log/apache2/access.log** and running `md5sum` on the file.


### 3. It is believed that a credential dumping tool was downloaded? What is the file name of the download?
> mimikatz_trunk.zip


After navigating to **/root/downloads/**, you'll see only one file listed in there.

### 4. There was a super-secret file created. What is the absolute path?
> /root/Desktop/SuperSecretFile.txt

Initially I extracted the *root* user's home directory and ran `find . -iname "*.txt" from within, but there were no results which matched the hint given in the text input.

I moved onto question 11, at which point I had an idea that the command to write this file was within `.bash_history`. Lo and behold, right at the top of the file was the answer.

![Shh, it's a secret](./images/04-Secret_File.png)


### 5. What program used didyouthinkwedmakeiteasy.jpg during execution?
> binwalk

This was quite an easy one to answer now that I was using the `.bash_history` more.

![binwalk](./images/05-binwalk.png)


### 6. What is the third goal from the checklist Karen created?
> profit

Taking a look at the file in `root/Desktop/Checklist` will give you the answer to this.


### 7. How many times was apache run?
> 0

### 8. It is believed this machine was used to attack another. What file proves this?
> irZLAohL.jpeg

Looking at this file in the *root* users home directory, you can see what is probably a screenshot of a remote desktop session.


### 9. Within the Documents file path, it is believed that Karen was taunting a fellow computer expert through a bash script. Who was Karen taunting?
> young

The file `root/Documents/myfirsthack/firstscript_fixed` has the line which contains this person's name.


### 10. A user su'd to root at 11:26 multiple times. Who was it?
> postgres

I extraced the contents of the `log` directory and ran the command `grep -rin '11:26' .` and found that the user *postgres* had attempted to elevate its privileges quite a few times.

![su](./images/10-postgres.png)


### 11. Based on the bash history, what is the current working directory?
> /root/Documents/myfirsthack/

Looking at the bottom of the `.bash_history` file, there's some directory traversal, but the user finally settles on `/root/Documents/myfirsthack/`:

![The Directory](./images/11-Directory.png)
