# [L'espion](https://cyberdefenders.org/labs/73)

#### Scenario
You have been tasked by a client whose network was compromised and brought offline to investigate the incident and determine the attacker's identity.

Incident responders and digital forensic investigators are currently on the scene and have conducted a preliminary investigation. Their findings show that the attack originated from a single user account, probably, an insider.

Investigate the incident, find the insider, and uncover the attack actions.


---
## Questions
### 1. File -> Github.txt: What is the API key the insider added to his GitHub repositories?
> `aJFRaLHjMXvYZgLPwiJkroYLGRkNBW`

Taking a look at **Github.txt**, there's a link to a [Github user](https://github.com/EMarseille99).
s
