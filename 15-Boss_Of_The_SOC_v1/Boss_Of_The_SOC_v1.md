# [Boss Of The SOC v1](https://cyberdefenders.org/labs/15)


#### Scenario 1
The focus of this hands on lab will be an APT scenario and a ransomware scenario. You assume the persona of Alice Bluebird, the analyst who has recently been hired to protect and defend Wayne Enterprises against various forms of cyberattack.

In this scenario, reports of the below graphic come in from your user community when they visit the Wayne Enterprises website, and some of the reports reference "P01s0n1vy." In case you are unaware, P01s0n1vy is an APT group that has targeted Wayne Enterprises. Your goal, as Alice, is to investigate the defacement, with an eye towards reconstructing the attack via the Lockheed Martin Kill Chain.


#### Scenario 2
In the second scenario, one of your users is greeted by this image on a Windows desktop that is claiming that files on the system have been encrypted and payment must be made to get the files back. It appears that a machine has been infected with Cerber ransomware at Wayne Enterprises and your goal is to investigate the ransomware with an eye towards reconstructing the attack.

---

## Contents
- [Scenario 1](#scenario-1)
- [Scenario 2](#scenario-2)
- [Questions](#questions)
    1. [Question 1](#question-1) - *What is the name of the company that makes the software that you are using for this competition?*
    2. [Question 2](#question-2) - *What is the likely IP address of someone from the Po1s0n1vy group scanning imreallynotbatman.com for web application vulnerabilities?*
    3. [Question 3](#question-3) - *What company created the web vulnerability scanner used by Po1s0n1vy?*
    4. [Question 4](#question-4) - *What content management system is imreallynotbatman.com likely using?*
    5. [Question 5](#question-5) - *What is the name of the file that defaced the imreallynotbatman.com website?*
    6. [Question 6](#question-6) - *What fully qualified domain name (FQDN) is associated with this attack?*
    7. [Question 7](#question-7) - *What IP address has Po1s0n1vy tied to domains that are pre-staged to attack Wayne Enterprises?*
    8. [Question 8](#question-8) - *Based on the data gathered from this attack and common open source intelligence sources for domain names, what is the email address that is most likely associated with Po1s0n1vy APT group?*
    9. [Question 9](#question-9) - *What IP address is likely attempting a brute force password attack against imreallynotbatman.com?*
    10. [Question 10](#question-10) - *What is the name of the executable uploaded by Po1s0n1vy?*
    11. [Question 11](#question-11) - *What is the MD5 hash of the executable uploaded?*
    12. [Question 12](#question-12) - *GCPD reported that common TTPs (Tactics, Techniques, Procedures) for the Po1s0n1vy APT group, if initial compromise fails, is to send a spear phishing email with custom malware attached to their intended target.*
    13. [Question 13](#question-13) - *What special hex code is associated with the customized malware discussed in [question 12](#question-12)?*
    14. [Question 14](#question-14) - *One of Po1s0n1vy's staged domains has some disjointed "unique" whois information. Concatenate the two codes together and submit as a single answer.*
    15. [Question 15](#question-15) - **
    16. [Question 16](#question-16) - **
    17. [Question 17](#question-17) - **
    18. [Question 18](#question-18) - **
    19. [Question 19](#question-19) - **
    20. [Question 20](#question-20) - **
    21. [Question 21](#question-21) - **
    22. [Question 22](#question-22) - **
    23. [Question 23](#question-23) - **
    24. [Question 24](#question-24) - **
    25. [Question 25](#question-25) - **
    26. [Question 26](#question-26) - **
    27. [Question 27](#question-27) - **
    28. [Question 28](#question-28) - **
    29. [Question 29](#question-29) - **
    30. [Question 30](#question-30) - **
    31. [Question 31](#question-31) - **
    32. [Question 32](#question-32) - **

---
## Questions

### Question 1
###### This is a simple question to get you familiar with submitting answers. What is the name of the company that makes the software that you are using for this competition? Just a six-letter word with no punctuation.
> `Splunk`


### 2. What is the likely IP address of someone from the Po1s0n1vy group scanning imreallynotbatman.com for web application vulnerabilities?
> **Original Search Term:** `earliest=0 imreallynotbatman.com`
> **Answer:** `40.80.148.42`

Looking at the *src_ip* field, `40.80.148.42` has three times the amount of requests of the second IP address listed; happens to be a *Class C private address*.

![Bad IP!](./images/02-bad_ip.png)

> **Improved Search Term:** `earliest=0 imreallynotbatman.com | stats count by src_ip | sort - count`

![Improved Search](./images/02-bad_ip_improved.png)


### Question 3
###### What company created the web vulnerability scanner used by Po1s0n1vy? Type the company name. (For example "Microsoft" or "Oracle")
> **Search Term:** `imreallynotbatman.com src_ip="40.80.148.42"`
> **Answer:** `acunetix`

Adding the IP address to the search term I gained from [question 2](#question-2) meant that I was able to narrow down the haystack in which I was searching. The first result of the search had some intereting fields towards the bottom, in particular **Acunetix-Product** and **Acunetix-Scanning-agreement**. This tells me the company that makes the vulnerability scanner.

![Scanning](./images/03-web_scanner.png)


### Question 4
###### What content management system is imreallynotbatman.com likely using? (Please do not include punctuation such as . , ! ? in your answer. We are looking for alpha characters only.)
> **Search Term:** `imreallynotbatman.com src_ip="40.80.148.42"`
> **Answer:** `joomla`

Using the same term [question 3](#question-3), I took a look at the *src_headers* and found the answer.

![CMS](./images/04-cms.png)


### Question 5
###### What is the name of the file that defaced the imreallynotbatman.com website? Please submit only the name of the file with extension (For example "notepad.exe" or "favicon.ico").
> **Search Term:** ``
> **Answer:** `poisonivy-is-coming-for-you-batman.jpeg`

Can't remember how I got this answer.



### Question 6
###### This attack used dynamic DNS to resolve to the malicious IP. What fully qualified domain name (FQDN) is associated with this attack?
> **Search Term:** `earliest=0 sourcetype="stream:http" poisonivy-is-coming-for-you-batman.jpeg`
> **Answer:** `prankglassinebracket.jumpingcrab.com`

Looking the results from [question 5](#question-5), I can see the domain name under the *site* header.

![Domain](./images/06-domain.png)


### Question 7
###### What IP address has Po1s0n1vy tied to domains that are pre-staged to attack Wayne Enterprises?
> **Search Term:** `earliest=0 sourcetype="stream:http" poisonivy-is-coming-for-you-batman.jpeg`
> **Answer:** `23.22.63.114`

Using the same result from [question 6](#question-6), I can see the IP address under the *dest_ip* header.

![Pre-staged IP](./images/07-pre_stage.png)


### Question 8
###### Based on the data gathered from this attack and common open source intelligence sources for domain names, what is the email address that is most likely associated with Po1s0n1vy APT group?
> `lillian.rose@po1s0n1vy.com`

This question was quite difficult to answer. I tried many different tools:
 * Harvester
 * Maltego
 * Recon-NG
 * [virustotal](https://www.virustotal.com)
 * [www.robtex](https://www.robtex.com)

I had to look up a guide quickly to see what I was missing, and with that I found that someone was using [Threat Crowd](https://threatcrowd.org/) to do the look ups.

I first tried to search the domain name gained in [question 6](#question-6), but only got IP addresses and other domains associated with the domain.

![Threat Crowd - Domain](./images/08-threat_crowd_domain.png)

Next I tried the IP address from the [previous question](#question-7), and managed to find the email address hidden amongst the spurs.

![Threat Crowd - IP](./images/08-threat_crowd_ip.png)


### Question 9
###### What IP address is likely attempting a brute force password attack against imreallynotbatman.com?
> **Search Term:** `earliest=0 source="stream:http" imreallynotbatman.com http_method="POST" "*passwd=*" | stats count by c_ip, form_data`
> **Answer:** `23.22.63.114`

My initial search term was `earliest=0 source="stream:http" imreallynotbatman.com http_method="POST" password`, which whilst didn't give me an immediate answer to the question, it did however give me the HTML with the login form. From this form, I was able to find the name of password input field: `passwd`.

![Login Form](./images/09-passwd_form.png)

Searching for the term `*passwd=*` instead of `password` meant that I was able to filter out everything except what I was looking for.

![Password Brute Force](./images/09-password.png)


### Question 10
###### What is the name of the executable uploaded by Po1s0n1vy? Please include file extension. (For example, "notepad.exe" or "favicon.ico")
> **Search Term:** `earliest=0 source="stream:http" imreallynotbatman.com upload`
> **Answer:** `3791.exe`

I played around with many different search terms before I settled on a fairly simple one which gave me the answer within the first result.

![Upload](./images/10-upload.png)


### Question 11
###### What is the MD5 hash of the executable uploaded?
> **Search Term:** `earliest=0 3791.exe CommandLine="3791.exe"`
> **Answer:** `AAE3F5A29935E6ABCC2C2754D12A9AF0`

My first search term was `earliest=0 3791.exe md5`. There were quite a few results returned, but none on the first page yielded the correct answer.

I took a closer look, and noticed that some of the commands were children of the `3791.exe` process. So I updated my search term to `earliest=0 3791.exe CommandLine="3791.exe"` and was able to narrow it down to only one result.

![MD5 Hash](./images/11-md5_hash.png)

Searching around on the internet, I found a better search term which would have given me just the MD5 hash without all of the other data by using `rex` (**regular expression**):

`earliest=0 3791.exe CommandLine="3791.exe" | rex field=_raw MD5="(?<md5sum>\w+)" | table md5sum`

![Improved Search Term](./images/11-improved.png)


### Question 12
###### GCPD reported that common TTPs (Tactics, Techniques, Procedures) for the Po1s0n1vy APT group, if initial compromise fails, is to send a spear phishing email with custom malware attached to their intended target. This malware is usually connected to Po1s0n1vys initial attack infrastructure. Using research techniques, provide the SHA256 hash of this malware.
> **Answer:** `9709473ab351387aab9e816eff3910b9f28a7a70202e250ed46dba8f820f34a8`

I started off by taking the MD5 hash found in [question 11](#question-11) and searching using [VirusTotal](https://www.virustotal.com). Unfortunately, the SHA256 which was registered along with the executable was not the correct answer.

![Wrong Hash](./images/12-md5_search.png)

Next up, I searched for the IP address found in [question 9](#question-9) in VirusTotal. This gave me a little more, especially in the `Relations` section of the result.

![IP Search](./images/12-ip_search.png)

Three seperate files are listed; `ab.exe`, `check.exe`, and `MirandaTateScreensaver.scr.exe`. `ab.exe` was the file returned by searching for the MD5 hash. `check.exe` wasn't the correct executable. Where as the `MirandaTateScreensaver.scr.exe` provided the correct SHA256 hash.

![Correct Hash](./images/12-ip_hash.png)


### Question 13
###### What special hex code is associated with the customized malware discussed in [question 12](#question-12)? (Hint: It's not in Splunk)
> **Answer:** `53 74 65 76 65 20 42 72 61 6e 74 27 73 20 42 65 61 72 64 20 69 73 20 61 20 70 6f 77 65 72 66 75 6c 20 74 68 69 6e 67 2e 20 46 69 6e 64 20 74 68 69 73 20 6d 65 73 73 61 67 65 20 61 6e 64 20 61 73 6b 20 68 69 6d 20 74 6f 20 62 75 79 20 79 6f 75 20 61 20 62 65 65 72 21 21 21`

Clicking on the `Community` tab from the same file found in [question 12](#question-12), I was able to get the information from someone who added it as a comment.

![Hex](./images/13-hex.png)


### Question 14
###### One of Po1s0n1vy's staged domains has some disjointed "unique" whois information. Concatenate the two codes together and submit as a single answer.
