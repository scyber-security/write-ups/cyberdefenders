# [Brave](https://cyberdefenders.org/labs/67)

#### Scenario
A memory image was taken from a seized Windows machine. Analyze the image and answer the provided questions.

---
## Questions

### 1. What time was the RAM image acquired according to the suspect system? (YYYY-MM-DD HH:MM:SS)
> `2021-04-30 17:52:19`

I ran the command `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.info` which gave me information about the image, for example the *System Time* of when the image was taken.

![Image Info](./images/01-info.png)


### 2. What is the SHA256 hash value of the RAM image?
> `9db01b1e7b19a3b2113bfb65e860fffd7a1630bdf2b18613d206ebf2aa0ea172`

Ran `sha256sum 20210430-Win10Home-20H2-64bit-memdump.mem`.


### 3. What is the process ID of "brave.exe"?
> `4856`

I ran the command `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.pslist.PsList | grep -i brave` which returned the PID for the running brave instance.

![Brave PID](./images/03-brave_pid.png)


### 4. How many established network connections were there at the time of acquisition? (number)
> `10`

I ran the command `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.netscan.NetScan | grep ESTABLISHED | wc -l` which gave me the number of *established* connections.

![Established Connections](./images/04-connections.png)


### 5. What FQDN does Chrome have an established network connection with?
> `mail.protonmail.com`

I ran the command `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.netscan.NetScan | grep ESTABLISHED > established.txt` to get a list of the established connections. This gave me the IP address of **185.70.41.130**.

![IPs](./images/05-ips.png)

I did a reverse IP lookup, it came back with `protonmail.ch`. This unfortunately was not the correct answer.

![Wrong FQDN](./images/05-wrong_fqdn.png)

I decided to try the IP address, and bingo! The initial page is redirected to `mail.protonmail.com` before it gets redirected again to `account.protonmail.com`.

![Redirect](./images/05-mail.png)


### 6. What is the MD5 hash value of process memory for PID 6988?
> `0b493d8e26f03ccd2060e0be85f430af`

Running the command `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.pslist.PsList --pid 6988 --dump` filtered the output of the PID and dumped it to the local disk. Then running `md5dsum` on the file provided me with the answer.

![PID MD5](./images/06-pid_md5.png)


### 7. What is the word starting at offset 0x45BE876 with a length of 6 bytes?
> `hacker`

In order to get the word, I had to load the memory dump into a hex editor. Luckily Linux comes with one *(typically)* install out of the box. Running the command `xxd -s 0x45BE876 -l 6 20210430-Win10Home-20H2-64bit-memdump.mem` gave me the word which.

![The Word](./images/07-word.png)


### 8. What is the creation date and time of the parent process of "powershell.exe"? (YYYY-MM-DD HH:MM:SS)
> `2021-04-30 17:39:48`

I first ran `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.pslist.PsList` to get the list of processes, and fortunately found **powershell.exe** at the bottom of the list.

![PowerShell](./images/08-powershell.png)

The parent process of **powershell.exe** has a PID of `5096` with its parent process having a PID of `4352`. Scrolling up through the list of processes until I found the process with the PID of `4352`, which happens to be **explorer.exe**.

![explorer](./images/08-explorer.png)

I did try to run `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.pstree.PsTree --pid 5096`, however this never completed.


### 9. What is the full path and name of the last file opened in notepad?
> `C:\Users\JOHNDO~1\AppData\Local\Temp\7zO4FB31F24\accountNum`

Using the same output from the `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.pslist.PsList`, I searched for **notepad.exe**; this gave me the PID of `2520`.

![notepad](./images/09-notepad.png)

Once I found the PID of notepad, I tried a number of different plugins:
* `windows.handles.Handles` - Gave me a long list of file handles owned by notepad, but too long to know what to filter out.
* `windows.filescan.FileScan` - Gave me a long list of open files but no way of filtering by PID.

I was able to run the command `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.cmdline.CmdLine --pid 2520` to fiter out everything except the notepad process. This gave me the answer to the question.

![The File](./images/09-file.png)


### 10. How long did the suspect use Brave browser? (hh:mm:ss)
> `04:01:54`

After a little bit of research, I stumbled across this page: `https://www.andreafortuna.org/2018/05/23/forensic-artifacts-evidences-of-program-execution-on-windows-systems/`. In it, it mentions something about **UserAssist** where application usage is tracked inside of the Windows registry. Taking a look at the plugins available for **Volatility 3**, I can see there is in fact one for the UserAssist.

Running the command `vol3 -f 20210430-Win10Home-20H2-64bit-memdump.mem windows.registry.userassist.UserAssist` gave me a long list of items, so I filtered them out just looking for anything related to brave.

![Brave](./images/10-brave.png)

Not knowing what the columns were, I printed the top few lines using the `head` command.

![head](./images/10-head.png)

The column **Time Focused** is what I was looking for in this instance.
